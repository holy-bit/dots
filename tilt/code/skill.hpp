#ifndef SKILL_H
#define SKILL_H
#include "Sprite.hpp"

#include <basics/Id>
#include <basics/Timer>

namespace example
{
    using basics::Id;
    using basics::Timer;

    class skill
    {
        typedef std::shared_ptr < Sprite     >     Sprite_Handle;


    public:
        ///Inicializa la skill
        skill();

        void update(float time);

        /**
         * Controla el tiempo de accion de la explosion de la habilidad.
         */
        void explosion();

        /**
         * resetea el objeto para auto generarlo automaticamente.
         */
        void reset();

        // Getters autoexplicativos:

        const bool    & get_active      () const { return  active;  }
        const std::shared_ptr < Sprite     >   & get_icon      () const { return  icon;  }
        const std::shared_ptr < Sprite     >   & get_exp      () const { return  exp;  }


    private:
        static constexpr float   speed = 50.f; ///< Velocidad a la que se mueve el la skill (en unideades virtuales por segundo).
        std::shared_ptr < Sprite     > icon; //Sprite de la habilidad.
        std::shared_ptr < Sprite     > exp;  //Sprite de la explosion.
        bool active;                         //comprueba si esta activa.
        Timer          timer;
    };



}
#endif