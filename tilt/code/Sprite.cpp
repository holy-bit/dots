/*
 * SPRITE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sprite.hpp"

using namespace basics;

namespace example
{

    Sprite::Sprite(Texture_2D * texture)
    :
        texture (texture)
    {
        anchor   = basics::CENTER;
        size     = { texture->get_width (), texture->get_height () };
        position = { 0.f, 0.f };
        scale    = 1.f;
        speed    = { 0.f, 0.f };
        visible  = true;
        angle=0;
    }
    ///Comprueba las colisiones entre dos sprites considerando su colision circular.
    bool Sprite::intersectsCircle (const Sprite & other)
    {
        float  delta_x  = this->get_position_x()-other.get_position_x();
        float  delta_y  = this->get_position_y() - other.get_position_y();
        float  distance = delta_x * delta_x + delta_y * delta_y;
        float radius = this->get_position_y()-this->get_bottom_y();
        float radius2 = other.get_position_y()-other.get_bottom_y();
        return distance < radius* radius + radius2 * radius2;
    }
    bool Sprite::intersects (const Sprite & other)
    {
        // Se determinan las coordenadas de la esquina inferior izquierda y de la superior derecha
        // de este sprite:

        float this_left    = this->get_left_x   ();
        float this_bottom  = this->get_bottom_y ();
        float this_right   = this_left   + this->size.width * scale;
        float this_top     = this_bottom + this->size.height *scale;

        // Se determinan las coordenadas de la esquina inferior izquierda y de la superior derecha
        // del otro sprite:

        float other_left   = other.get_left_x   ();
        float other_bottom = other.get_bottom_y ();
        float other_right  = other_left   + other.size.width *scale;
        float other_top    = other_bottom + other.size.height *scale;

        // Se determina si los rectángulos envolventes de ambos sprites se solapan:

        return !(other_left >= this_right || other_right <= this_left || other_bottom >= this_top || other_top <= this_bottom);
    }

    /// Comprueba si un punto se encuentra dentro del sprite.
    bool Sprite::contains (const Point2f & point)
    {
        float this_left = this->get_left_x ();

        if (point.coordinates.x () > this_left)
        {
            float this_bottom = this->get_bottom_y ();

            if (point.coordinates.y () > this_bottom)
            {
                float this_right = this_left + this->size.width*scale;

                if (point.coordinates.x () < this_right)
                {
                    float this_top = this_bottom + this->size.height*scale;

                    if (point.coordinates.y () < this_top)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

}
