#include "enemy.hpp"

#include "Game_Scene.hpp"

namespace example {

    constexpr float enemy::speed;

    //Crea el sprite
    enemy::enemy(){
        sprite = Game_Scene::getInstance()->addSprite(ID(ball),Game_Scene::getInstance()->random_position(),0.5f);
    }

    //Actualiza el la direccion del enemigo cuando pasan 1.5s de su aparicion
    void enemy::update(float time) {
        if(timer.get_elapsed_seconds()>1.5f)
        sprite->set_speed (getDirection().normalized () * speed);
    }

    //Obtiene el vector director a la posicion del jugador.
    Vector2f enemy::getDirection() {
        return Vector2f
                (
                        Game_Scene::getInstance()->getPlayer()->get_position_x() - sprite->get_position_x(),
                        Game_Scene::getInstance()->getPlayer()->get_position_y() - sprite->get_position_y()
                );

    }

}
