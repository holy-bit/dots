/*
 * GAME SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Game_Scene.hpp"

#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Accelerometer>
#include <future>
#include <sstream>
#include <basics/Log>
#include <math.h>
#include "Menu_Scene.hpp"


using namespace basics;
using namespace std;

namespace example
{
    // ---------------------------------------------------------------------------------------------
    // ID y ruta de las texturas que se deben cargar para esta escena. La textura con el mensaje de
    // carga está la primera para poder dibujarla cuanto antes:

    Game_Scene::Texture_Data Game_Scene::textures_data[] =
    {
        { ID(loading),    "game-scene/loading.png"        },
        { ID(hbar),       "game-scene/horizontal-bar.png" },
        { ID(vbar),       "game-scene/vertical-bar.png"   },
        { ID(player-bar), "game-scene/players-bar.png"    },
        { ID(ball),       "game-scene/ball.png"           },
        { ID(arrow),       "game-scene/arrow.png"         },
        { ID(play),       "game-scene/play.png"           },
        { ID(retry),       "game-scene/retry.png"           },
        { ID(quit),       "game-scene/quit.png"           },
        { ID(skill),       "game-scene/skill.png"  }
    };




    // Pâra determinar el número de items en el array textures_data, se divide el tamaño en bytes
    // del array completo entre el tamaño en bytes de un item:

    unsigned Game_Scene::textures_count = sizeof(textures_data) / sizeof(Texture_Data);

    // ---------------------------------------------------------------------------------------------
    // Definiciones de los atributos estáticos de la clase:



    std::shared_ptr< Game_Scene > Game_Scene::instance;

    // ---------------------------------------------------------------------------------------------

    Game_Scene::Game_Scene()
    {
        // Se establece la resolución virtual (independiente de la resolución virtual del dispositivo).
        // En este caso no se hace ajuste de aspect ratio, por lo que puede haber distorsión cuando
        // el aspect ratio real de la pantalla del dispositivo es distinto.

        canvas_width  = 1280;
        canvas_height =  720;

        // Se inicia la semilla del generador de números aleatorios:

        srand (unsigned(time(nullptr)));


        // Se inicializan otros atributos:

        initialize ();
    }

    std::shared_ptr< Game_Scene > Game_Scene::getInstance()
    {
        if (!instance)
        {
            instance.reset (new Game_Scene);
        }

        return instance;
    }

    // ---------------------------------------------------------------------------------------------
    // Algunos atributos se inicializan en este método en lugar de hacerlo en el constructor porque
    // este método puede ser llamado más veces para restablecer el estado de la escena y el constructor
    // solo se invoca una vez.

    bool Game_Scene::initialize ()
    {

        state     = LOADING;
        suspended = true;
        gameplay  = UNINITIALIZED;

        return true;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::suspend ()
    {
        suspended = true;               // Se marca que la escena ha pasado a primer plano
        Accelerometer * accelerometer = Accelerometer::get_instance ();

        if (accelerometer) accelerometer->switch_off ();
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::resume ()
    {
        suspended = false;              // Se marca que la escena ha pasado a segundo plano
        Accelerometer * accelerometer = Accelerometer::get_instance ();

        if (accelerometer) accelerometer->switch_on ();
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::handle (Event & event)
    {
    if (state == RUNNING || state == PAUSE)               // Se descartan los eventos cuando la escena está LOADING
        {

            switch (event.id)
            {
                case ID(touch-started):{
                    if(state==PAUSE){
                        user_target_y = *event[ID(y)].as< var::Float > ();
                        user_target_x = *event[ID(x)].as< var::Float > ();// El usuario toca la pantalla
                    }
                    else if(state == RUNNING){
                        state = PAUSE;
                        user_target_y=0;
                        user_target_x=0;
                    }
                    break;
                }
                case ID(touch-moved):


                case ID(touch-ended): {}      // El usuario deja de tocar la pantalla

            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::update (float time)
    {
        if (!suspended) switch (state)
        {
            case LOADING: load_textures  ();     break;
            case RUNNING: run_simulation (time); break;
            case PAUSE:   pauseMenu(time); break;
            case ERROR:   break;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render (Context & context)
    {
        if (!suspended)
        {
            // El canvas se puede haber creado previamente, en cuyo caso solo hay que pedirlo:

            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            // Si no se ha creado previamente, hay que crearlo una vez:

            if (!canvas)
            {
                 canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            // Si el canvas se ha podido obtener o crear, se puede dibujar con él:

            if (canvas)
            {
                canvas->clear ();

                switch (state)
                {
                    case LOADING: render_loading   (*canvas); break;
                    case RUNNING: render_playfield (*canvas); break;
                    case PAUSE: render_playfield (*canvas); break;
                    case ERROR:   break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    // En este método solo se carga una textura por fotograma para poder pausar la carga si el
    // juego pasa a segundo plano inesperadamente. Otro aspecto interesante es que la carga no
    // comienza hasta que la escena se inicia para así tener la posibilidad de mostrar al usuario
    // que la carga está en curso en lugar de tener una pantalla en negro que no responde durante
    // un tiempo.

    void Game_Scene::load_textures ()
    {
        if (textures.size () < textures_count)          // Si quedan texturas por cargar...
        {
            // Las texturas se cargan y se suben al contexto gráfico, por lo que es necesario disponer
            // de uno:

            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                // Se carga la siguiente textura (textures.size() indica cuántas llevamos cargadas):

                Texture_Data   & texture_data = textures_data[textures.size ()];
                Texture_Handle & texture      = textures[texture_data.id] = Texture_2D::create (texture_data.id, context, texture_data.path);

                // Se comprueba si la textura se ha podido cargar correctamente:

                if (texture) context->add (texture); else state = ERROR;

                // Cuando se han terminado de cargar todas las texturas se pueden crear los sprites que
                // las usarán e iniciar el juego:
            }
        }
        else
        if (timer.get_elapsed_seconds () > 1.f)         // Si las texturas se han cargado muy rápido
        {                                               // se espera un segundo desde el inicio de
            create_sprites ();                          // la carga antes de pasar al juego para que
            restart_game   ();                          // el mensaje de carga no aparezca y desaparezca
                                                        // demasiado rápido.
            state = RUNNING;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::create_sprites ()
    {
        // Se crean y configuran los sprites del fondo:

        Sprite_Handle    top_bar(new Sprite( textures[ID(hbar)].get () ));

        Sprite_Handle bottom_bar(new Sprite( textures[ID(hbar)].get () ));
        Sprite_Handle left_bar(new Sprite( textures[ID(vbar)].get () ));
        Sprite_Handle right_bar(new Sprite( textures[ID(vbar)].get () ));

           top_bar->set_anchor   (TOP | LEFT);
           top_bar->set_position ({ 0, canvas_height });

        bottom_bar->set_anchor   (BOTTOM | LEFT);
        bottom_bar->set_position ({ 0, 0 });
        left_bar->set_anchor   (LEFT);
        left_bar->set_position ({ 0, canvas_height / 2.f });
        right_bar->set_anchor   (RIGHT);
        right_bar->set_position ({ 1280, canvas_height / 2.f });
        sprites.push_back (   left_bar);
        sprites.push_back (   right_bar);
        sprites.push_back (   top_bar);
        sprites.push_back (bottom_bar);

        // Se crean los players y la bola:


        Sprite_Handle ball_handle(new Sprite( textures[ID(arrow)      ].get () ));
        ball_handle->set_scale(0.1f);
        sprites.push_back (ball_handle);
        Sprite_Handle plays(new Sprite( textures[ID(play)].get () ));
        plays->set_position({canvas_width/2-200,canvas_height/2});
        plays->hide();
        Sprite_Handle restart(new Sprite( textures[ID(retry)].get () ));
        restart->set_position({canvas_width/2,canvas_height/2});
        restart->hide();
        Sprite_Handle close(new Sprite( textures[ID(quit)].get () ));
        close->set_position({canvas_width/2+200,canvas_height/2});
        close->hide();
        sprites.push_back (plays);
        sprites.push_back (restart);
        sprites.push_back (close);
        // Se guardan punteros a los sprites que se van a usar frecuentemente:
        play = plays.get();
        retry = restart.get();
        quit = close.get();
        top_border    =             top_bar.get ();
        bottom_border =          bottom_bar.get ();
        left_border   =             left_bar.get ();
        right_border =          right_bar.get ();

        player.sprite          =         ball_handle;
    }

    ///Resetea toda la escena a una situacion inicial de la partida.
    void Game_Scene::restart_game()
    {
        player.sprite->set_position ({ canvas_width / 2.f, canvas_height / 2.f });
        player.sprite->set_speed    ({ 0.f, 0.f });

        for(Enemy_Handle& aux : enemies){
            aux->get_sprite()->hide();
        }
        enemies.clear();

        for(skill& s:skills2){
            s.reset();
        }



        gameplay = PLAYING;
    }

    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------

    //Funciona como update del gameplay
    void Game_Scene::run_simulation (float time)
    {
        // Se actualiza el estado de todos los sprites:

        for (auto & sprite : sprites)
        {
            sprite->update (time);
        }

        // Se actualiza el estado de todos las habilidades:
        for(skill& s:skills2) {
                s.update(time);
        }

        // Cada segundo se crea un nuevo enemigo
        if(timer.get_elapsed_seconds()>1){

            enemies.push_back(Enemy_Handle(new enemy()));
            timer.reset();
        }

        // Se actualiza el estado de todos los enemigos:
        for(Enemy_Handle& aux : enemies){

            aux->update(time);
            //aux.x+=5;
        }
        // Se actualiza el estado del jugador.
        player.update(time);


    }



    // ---------------------------------------------------------------------------------------------
    ///Genera una direcion aleatoria.
    Vector2f Game_Scene::random_direction() {
        return Vector2f
                {
                        float(rand() % int(canvas_width) - int(canvas_width / 2)),
                        float(rand() % int(canvas_height) - int(canvas_height / 2))
                };
    }

    // --------------------------------------------------------------------------------------------------------------------------

    ///Genera una posicion aleatoria.
    Vector2f Game_Scene::random_position(){
        return Vector2f {float(rand () % int(canvas_width )), float(rand () % int(canvas_height ))};
    }


    // --------------------------------------------------------------------------------------------------------------------------
    //Se llama cuando el juego entra en estado de PAUSE el cual se para si se muestran tres botones con distintas opcines.
    void Game_Scene::pauseMenu(float time) {
        play->show();
        retry->show();
        quit->show();
        play->update(time);
        retry->update(time);
        quit->update(time);
        if(play->contains({user_target_x,user_target_y})){
            play->hide();
            retry->hide();
            quit->hide();
            state=RUNNING;
        }
        if(retry->contains({user_target_x,user_target_y})){
            play->hide();
            retry->hide();
            quit->hide();
            restart_game();
            state=RUNNING;
        }
        if(quit->contains({user_target_x,user_target_y})){

            director.run_scene (shared_ptr< Scene >(new Menu_Scene));
        }
    }

    // ---------------------------------------------------------------------------------------------
    //Carga todos los sprites
    void Game_Scene::render_loading (Canvas & canvas)
    {
        Texture_2D * loading_texture = textures[ID(loading)].get ();

        if (loading_texture)
        {
            canvas.fill_rectangle
            (
                { canvas_width * .5f, canvas_height * .5f },
                { loading_texture->get_width (), loading_texture->get_height () },
                  loading_texture
            );
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Simplemente se dibujan todos los sprites que conforman la escena.

    void Game_Scene::render_playfield (Canvas & canvas)
    {
        for (auto & sprite : sprites)
        {
            sprite->render (canvas);
        }
    }

}
