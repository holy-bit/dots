#ifndef PLAYER_H
#define PLAYER_H

#include "enemy.hpp"


namespace example
{

    class player
    {
        typedef std::shared_ptr < enemy     >     Enemy_Handle;
        typedef std::shared_ptr < Sprite     >     Sprite_Handle;

    public:
        static constexpr float   speed = 20.f; ///< Velocidad a la que se mueve el jugador (en unideades virtuales por segundo).
        Sprite_Handle sprite;
    public:

        player();
        void update(float time);

    };

}
#endif