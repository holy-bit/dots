#include "player.hpp"
#include "Game_Scene.hpp"
#include <basics/Accelerometer>



using namespace basics;


namespace example {

    constexpr float player::speed;
    player::player() {

    }

    void player::update(float time) {
        for(Enemy_Handle& aux : Game_Scene::getInstance()->getEnemies()){
            if(aux->get_sprite()->get_speed()!=Vector2f{0,0})
                if(sprite->intersectsCircle(*aux->get_sprite()) && aux->get_sprite()->is_visible()) Game_Scene::getInstance()->restart_game();
        }
        Accelerometer * accelerometer = Accelerometer::get_instance ();
        if (accelerometer)
        {
            const Accelerometer::State & acceleration = accelerometer->get_state ();

            float angle2;

            float roll  = atan2f ( acceleration.y, acceleration.z) * 57.3f;
            float pitch = atan2f (-acceleration.x, sqrtf (acceleration.y * acceleration.y + acceleration.z * acceleration.z)) * 57.3f;
            Vector2f direction {roll,pitch};


            float delta= pitch/roll;
            if(roll!=0 && pitch!=0){


                angle2 = atan(delta);
                if(roll<0) angle2+=180;
            }
            else if(pitch==0) angle2=roll;
            else if(roll==0)  angle2=pitch;
            sprite->set_angle(angle2);

            sprite->set_speed(direction *speed);

            if (sprite->get_position_x()-sprite->get_width()/2  <  15 && sprite->get_speed_x()<0) sprite->set_position_x(sprite->get_width()/2 ) ; else
            if (sprite->get_position_x()+sprite->get_width()/2 >= Game_Scene::getInstance()->getCanvasWidth()-15 && sprite->get_speed_x()>0) sprite->set_position_x(Game_Scene::getInstance()->getCanvasWidth() - sprite->get_width()/2);
            if (sprite->get_position_y()-sprite->get_width()/2  <  15 && sprite->get_speed_y()<0) sprite->set_position_y(sprite->get_width()/2 ) ; else
            if (sprite->get_position_y()+sprite->get_width()/2 >= Game_Scene::getInstance()->getCanvasHeight()-15 && sprite->get_speed_y()>0) sprite->set_position_y(Game_Scene::getInstance()->getCanvasHeight() - sprite->get_width()/2);

        }
    }
}
