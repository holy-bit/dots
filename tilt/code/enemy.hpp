#ifndef ENEMY_H
#define ENEMY_H

#include "Sprite.hpp"
#include <basics/Timer>
#include <basics/Id>


namespace example
{
    using basics::Timer;
    class enemy
    {
        typedef std::shared_ptr < Sprite     >     Sprite_Handle;

    public:

        ///Inicializa y crea el spite
        enemy();

        /**
         * Gestiona que a partir de 1.5s segun su aparicion persiga al jugador siempre
         * @param time
         */
        void update(float time);

        ///Obtiene el vector director para dirigirse al jugador.
        Vector2f getDirection();

        //Getter autoexplicativo:

        const std::shared_ptr < Sprite     >   & get_sprite      () const { return  sprite;  }

    private:
        static constexpr float   speed = 50.f; ///< Velocidad a la que se mueve el enemigo (en unideades virtuales por segundo).
        Sprite_Handle sprite;
        Timer timer;


    };

}
#endif