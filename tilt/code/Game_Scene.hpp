/*
 * GAME SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#ifndef GAME_SCENE_HEADER
#define GAME_SCENE_HEADER

    #include <map>
    #include <list>
    #include <memory>

    #include <basics/Canvas>
    #include <basics/Id>
    #include <basics/Scene>
    #include <basics/Texture_2D>
    #include <basics/Timer>
#include "enemy.hpp"
    #include "Sprite.hpp"
    #include "skill.hpp"
#include "player.hpp"



    namespace example
    {

        using basics::Id;
        using basics::Timer;
        using basics::Canvas;
        using basics::Texture_2D;

        class Game_Scene : public basics::Scene
        {

            // Estos typedefs pueden ayudar a hacer el código más compacto y claro:

            typedef std::shared_ptr < Sprite     >     Sprite_Handle;
            typedef std::list< Sprite_Handle     >     Sprite_List;
            typedef std::shared_ptr < enemy     >     Enemy_Handle;
            typedef std::list< Enemy_Handle    >     Enemy_List;
            typedef std::shared_ptr < skill     >     skill_pointer;
            typedef std::vector< Sprite_Handle     >     Sprite_vector;
            typedef std::shared_ptr< Texture_2D  >     Texture_Handle;
            typedef std::map< Id, Texture_Handle >     Texture_Map;
            typedef basics::Graphics_Context::Accessor Context;

            /**
             * Representa el estado de la escena en su conjunto.
             */
            enum State
            {
                LOADING,
                RUNNING,
                PAUSE,
                ERROR
            };

            /**
             * Representa el estado del juego cuando el estado de la escena es RUNNING.
             */
            enum Gameplay_State
            {
                UNINITIALIZED,
                WAITING_TO_START,
                PLAYING,

            };

        private:

            /**
             * Array de estructuras con la información de las texturas (Id y ruta) que hay que cargar.
             */
            static struct   Texture_Data { Id id; const char * path; } textures_data[];
             struct   skills_data{Sprite_Handle icon; bool active;} skills[3];



            /**
             * Número de items que hay en el array textures_data.
             */
            static unsigned textures_count;

        private:



            static std::shared_ptr< Game_Scene > instance;

        private:

            State          state;                               ///< Estado de la escena.
            Gameplay_State gameplay;                            ///< Estado del juego cuando la escena está RUNNING.
            bool           suspended;                           ///< true cuando la escena está en segundo plano y viceversa.

            unsigned       canvas_width;                        ///< Ancho de la resolución virtual usada para dibujar.
            unsigned       canvas_height;                       ///< Alto  de la resolución virtual usada para dibujar.

            Texture_Map    textures;                            ///< Mapa  en el que se guardan shared_ptr a las texturas cargadas.
            Sprite_List    sprites;                             ///< Lista en la que se guardan shared_ptr a los sprites creados.
            Enemy_List enemies;                                 ///< Lista en la que se guardan shared_ptr a los enemigos creados.
            skill  skills2[3];                                  ///< Array de tamaño tres que gestionara el numero de skill que puede haber en pantalla.
            player player;


            Sprite       * play;
            Sprite       * retry;
            Sprite       * quit;
            Sprite       * top_border;                          ///< Puntero al sprite de la lista de sprites que representa el borde superior.
            Sprite       * bottom_border;                       ///< Puntero al sprite de la lista de sprites que representa el borde inferior.
            Sprite       * left_border;                          ///< Puntero al sprite de la lista de sprites que representa el borde superior.
            Sprite       * right_border;
                                         ///< Puntero al sprite de la lista de sprites que representa al jugador.


            float          user_target_y;                       ///< Coordenada Y hacia donde debe ir el player del usuario cuando este toca la pantalla.
            float          user_target_x;
            Timer          timer;                               ///< Cronómetro usado para medir intervalos de tiempo

        private:

            /**
             * Solo inicializa los atributos que deben estar inicializados la primera vez, cuando se
             * crea la escena desde cero.
             */
            Game_Scene();

        public:

            //variable estatica que guarda un puntero a la instancia del singleton de la escena.
            static std::shared_ptr< Game_Scene > getInstance();

            // Getters (con nombres autoexplicativos):


            Sprite* getPlayer(){return player.sprite.get();}
            Sprite* getTop(){return top_border;}
            Sprite* getBot(){return bottom_border;}
            Sprite* getLeft(){return left_border;}
            Sprite* getRight(){return right_border;}
            unsigned  getCanvasWidth(){return canvas_width;}

            unsigned getCanvasHeight(){return canvas_height;}


            Enemy_List getEnemies(){ return enemies;}


            /**
             * Este método lo llama Director para conocer la resolución virtual con la que está
             * trabajando la escena.
             * @return Tamaño en coordenadas virtuales que está usando la escena.
             */
            basics::Size2u get_view_size () override
            {
                return { canvas_width, canvas_height };
            }

            /**
             * Aquí se inicializan los atributos que deben restablecerse cada vez que se inicia la escena.
             * @return
             */
            bool initialize () override;

            /**
             * Este método lo invoca Director automáticamente cuando el juego pasa a segundo plano.
             */
            void suspend () override;

            /**
             * Este método lo invoca Director automáticamente cuando el juego pasa a primer plano.
             */
            void resume () override;

            /**
             * Este método se invoca automáticamente una vez por fotograma cuando se acumulan
             * eventos dirigidos a la escena.
             */
            void handle (basics::Event & event) override;

            /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * actualize su estado.
             */
            void update (float time) override;

            /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * dibuje su contenido.
             */
            void render (Context & context) override;

            /**
             * Añade un sprite a la escena teniendo en cuenta varios parametros
             * @param ID , de la textura que se quiera mostar
             * @param position , posicion en la pantalla
             * @param scale , escala del sprite
             * @param index , y si queremos que se ponga al principio o final de la lista de sprites
             * @return sprite, un shared ptr del sprite es devuelto.
             */

            Sprite_Handle addSprite(int ID, Vector2f position={0,0}, float scale=1,bool index=false){
                Sprite_Handle sprite(new Sprite( textures[ID].get()));
                sprite->set_scale(scale);
                sprite->set_position ( position);
                std::list<Sprite_Handle>::iterator it;
                it = sprites.begin();
                if(index)
                sprites.insert(it,sprite);
                else
                sprites.push_back(sprite);
                return sprite;
            }

            /**
             * devuelve una direccion aleatoria.
             * @return
             */
            Vector2f random_direction();

            /**
             * devuelve una posicion aleatoria.
             * @return
             */
            Vector2f random_position();

            /**
             * Se llama cada vez que se debe reiniciar el juego. En concreto la primera vez y cada
             * vez que un jugador pierde.
             */
            void restart_game ();

        private:

            /**
             * En este método se cargan las texturas (una cada fotograma para facilitar que la
             * propia carga se pueda pausar cuando la aplicación pasa a segundo plano).
             */
            void load_textures ();

            /**
             * En este método se crean los sprites cuando termina la carga de texturas.
             */
            void create_sprites ();



            /**
             * Cuando se ha reiniciado el juego y el usuario toca la pantalla por primera vez se
             * pone la bola en movimiento en una dirección al azar.
             */
            void start_playing ();

            /**
             * Actualiza el estado del juego cuando el estado de la escena es RUNNING.
             */
            void run_simulation (float time);


            /**
             * Hace que el player derecho se mueva hacia el punto de la pantalla que toca el usuario.
             */
            void update_user ();


            /**
             * Dibuja la textura con el mensaje de carga mientras el estado de la escena es LOADING.
             * La textura con el mensaje se carga la primera para mostrar el mensaje cuanto antes.
             * @param canvas Referencia al Canvas con el que dibujar la textura.
             */
            void render_loading (Canvas & canvas);

            /**
             * Dibuja la escena de juego cuando el estado de la escena es RUNNING.
             * @param canvas Referencia al Canvas con el que dibujar.
             */
            void render_playfield (Canvas & canvas);

            /**
             * Para el juego y muestra el menu de pausa con sus distintos botones.
             * @param time
             */
            void pauseMenu(float time);



        };

    }

#endif
