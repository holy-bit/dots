#include "skill.hpp"
#include "Game_Scene.hpp"
#include <future>

namespace example {

    constexpr float skill::speed;

    skill::skill() {
        active=false;
        icon = nullptr;

    }

    void skill::update(float time) {

        // si el sprite es un puntero nulo generamos el Sprite y le damos una direccion aleatoria para que se mueva por la pantalla
        if(icon == nullptr ){
            icon=Game_Scene::getInstance()->addSprite(ID(skill),Game_Scene::getInstance()->random_position(),2);
            active=false;
            icon->set_speed (Game_Scene::getInstance()->random_direction().normalized () * speed);

        }
        else if(active){  //si es activada por el jugador se hace explotar.
            explosion();
        }
        else if(icon->intersectsCircle(*Game_Scene::getInstance()->getPlayer()) && icon->is_visible()){ //Comprobamos la colision con el jugador, si se cumple se activa.
            icon->hide();
            exp = Game_Scene::getInstance()->addSprite(ID(ball),icon->get_position(),5,true);
            active=true;
            timer.reset();

        }

         //Se comprueban las colisiones con los bordes de la pantalla.

        else if (icon->intersects (*Game_Scene::getInstance()->getTop()))
        {
            icon->set_speed_y    (-std::abs(icon->get_speed_y ()));
        }

        else if (icon->intersects (*Game_Scene::getInstance()->getBot()))
        {
            icon->set_speed_y    (std::abs(icon->get_speed_y ()));
        }

        else if (icon->intersects (*Game_Scene::getInstance()->getLeft()))
        {
            icon->set_speed_x    (std::abs(icon->get_speed_x ()));
        }
        else if (icon->intersects (*Game_Scene::getInstance()->getRight()))
        {
            icon->set_speed_x    (-std::abs(icon->get_speed_x ()));
        }

    }

    //Gestiona la explosion de la habilidad.
    void skill::explosion() {
        if(timer.get_elapsed_seconds()>2){
            exp->hide();
            active=false;
            icon= nullptr;
        }
        for(std::shared_ptr < enemy     >& aux : Game_Scene::getInstance()->getEnemies()){
            if(exp->intersectsCircle(*aux->get_sprite())) aux->get_sprite()->hide();
        }
    }

    //Resetea la habilidad.
    void skill::reset(){
        if(icon!= nullptr)
            icon->hide();
        if(exp!= nullptr)
            exp->hide();
        icon= nullptr;
        exp= nullptr;
    }




}
